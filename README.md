# Fizz BuzzREST Server

It is a robust and production-ready Fizz-Buzz REST Server that follows the classic Fizz-Buzz logic. The server should expose a REST API endpoint, with the additional feature of a statistical endpoint.

## Build Instructions

To build and run the project, follow these steps:

1. Clone the repository: `git clone https://gitlab.com/akshay_123/fizz-buzz-rest-server.git`
2. Navigate to the project directory: `cd your-project`
3. Build the project using Maven: `mvn clean install`
4. Run the application: `mvn spring-boot:run`

Make sure you have Java and Maven installed on your machine.

## Third-Party Libraries

The project uses the following third-party libraries:

1. **Spring Boot:** A powerful and convention-over-configuration-based framework for building Java-based, production-grade Spring applications.

2. **Spring Data JPA:** Part of the larger Spring Data project, it simplifies data access using JPA and Hibernate.

3. **JUnit:** A widely-used testing framework for Java applications.

4. **Mockito:** A mocking framework for unit tests in Java.

5. **H2 Database (In-memory):** A lightweight, in-memory database that is particularly useful for testing and development.

6. **Lombok:** A Java library that helps reduce boilerplate code in the project, such as getters, setters, and constructors.

7. **Log4j2:** A powerful and flexible logging framework for Java applications.


## Database Configuration

The project is configured to use the H2 in-memory database. You can access the H2 console at [http://localhost:8080/h2-console](http://localhost:8080/h2-console) when the application is running.

## API Documentation

API documentation for this project can be found at https://gitlab.com/akshay_123/fizz-buzz-rest-server/-/blob/main/FizzBuzzRESTServer_API_Specifications_v1.0.pdf?ref_type=heads

Please refer to the documentation for details on how to interact with the API and understand the available endpoints.

## Additional Notes


---

Feel free to customize the template further based on your specific use of these libraries and any additional information you want to provide.

