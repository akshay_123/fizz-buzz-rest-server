package com.roche.fizzbuzz.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.roche.fizzbuzz.dto.GenericResponse;
import com.roche.fizzbuzz.entity.FizzBuzzData;
import com.roche.fizzbuzz.repository.FizzBuzzDataRepo;
import com.roche.fizzbuzz.service.impl.StatisticsServiceImpl;
import com.roche.fizzbuzz.util.CommonConstant;

@RunWith(SpringJUnit4ClassRunner.class)
public class StatisticsServiceTest {
	@InjectMocks
	private StatisticsServiceImpl statisticsServiceImpl;

	@Mock
	private FizzBuzzDataRepo fizzBuzzDataRepo;

	@Test
	public void testGetStatistics_Success() {
		when(fizzBuzzDataRepo.findFirstByOrderByRequestHitsDesc()).thenReturn(getOPtFizzBuzzData());
		GenericResponse response = statisticsServiceImpl.getStatistics();
		assertNotNull(response);
		assertEquals(CommonConstant.SUCCESS_MESSAGE, response.getResponseMessage());
	}

	@Test
	public void testGetStatistics_StatisticsDataNotAvailable() {
		when(fizzBuzzDataRepo.findFirstByOrderByRequestHitsDesc()).thenReturn(Optional.empty());
		GenericResponse response = statisticsServiceImpl.getStatistics();
		assertNotNull(response);
		assertEquals(CommonConstant.STAT_DATA_NOT_AVAILABLE_MESSAGE, response.getResponseMessage());
	}

	private Optional<FizzBuzzData> getOPtFizzBuzzData() {
		return Optional.ofNullable(
				FizzBuzzData.builder().localId(1).firstNumber(1).secondNumber(2).contraint(5).firstString("fizz")
						.secondString("buzz").requestHits(5).result("fizz,fizzbuzz,fizz,fizzbuzz,fizz").build());
	}

}
