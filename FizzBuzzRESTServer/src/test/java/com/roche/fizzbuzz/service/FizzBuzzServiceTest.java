package com.roche.fizzbuzz.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.roche.fizzbuzz.dto.FizzBuzzRequest;
import com.roche.fizzbuzz.dto.GenericResponse;
import com.roche.fizzbuzz.entity.FizzBuzzData;
import com.roche.fizzbuzz.repository.FizzBuzzDataRepo;
import com.roche.fizzbuzz.service.impl.FizzBuzzServiceImpl;
import com.roche.fizzbuzz.util.CommonConstant;

@RunWith(SpringJUnit4ClassRunner.class)
public class FizzBuzzServiceTest {

	@InjectMocks
	private FizzBuzzServiceImpl fizzBuzzServiceImpl;

	@Mock
	private FizzBuzzDataRepo FizzBuzzDataRepo;

	@Test
	public void testSaveFizzBuzzResult_Success() {
		// prepare request
		FizzBuzzRequest request = prepareRequest();
		GenericResponse response = fizzBuzzServiceImpl.saveFizzBuzzResult(request);
		assertNotNull(response);
		assertEquals(CommonConstant.SUCCESS_MESSAGE, response.getResponseMessage());
		assertEquals("fizz,fizzbuzz,fizz,fizzbuzz,fizz", response.getResult());
	}

	@Test
	public void testSaveFizzBuzzResult_SuccessDuplicateRequest() {
		// prepare request
		FizzBuzzRequest request = prepareRequest();
		// Mock create request methods
		when(FizzBuzzDataRepo.findByFirstNumberAndSecondNumberAndContraintAndFirstStringAndSecondString(
				request.getFirstNumber(), request.getSecondNumber(), request.getLimit(), request.getFirstString(),
				request.getSecondString())).thenReturn(getFizzBuzzData(request));

		GenericResponse response = fizzBuzzServiceImpl.saveFizzBuzzResult(request);
		assertNotNull(response);
		assertEquals(CommonConstant.SUCCESS_MESSAGE, response.getResponseMessage());
		assertEquals("fizz,fizzbuzz,fizz,fizzbuzz,fizz", response.getResult());
	}

	private FizzBuzzData getFizzBuzzData(FizzBuzzRequest request) {

		return FizzBuzzData.builder().localId(1).firstNumber(request.getFirstNumber())
				.secondNumber(request.getSecondNumber()).contraint(request.getLimit())
				.firstString(request.getFirstString()).secondString(request.getSecondString()).requestHits(1)
				.result("fizz,fizzbuzz,fizz,fizzbuzz,fizz").build();
	}

	private FizzBuzzRequest prepareRequest() {
		return FizzBuzzRequest.builder().firstNumber(1).secondNumber(2).limit(5).firstString("fizz")
				.secondString("buzz").build();
	}

}
