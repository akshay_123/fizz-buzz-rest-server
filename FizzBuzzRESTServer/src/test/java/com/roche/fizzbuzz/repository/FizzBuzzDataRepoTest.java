package com.roche.fizzbuzz.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.roche.fizzbuzz.entity.FizzBuzzData;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
public class FizzBuzzDataRepoTest {

	@Autowired
	private FizzBuzzDataRepo fizzBuzzDataRepo;

	@Test
	public void testSaveFzzBuzzData_Success() {
		FizzBuzzData data = fizzBuzzDataRepo.save(getFizzBuzzData(1));
		assertNotNull(data);
		assertEquals("fizz,fizzbuzz,fizz,fizzbuzz,fizz", data.getResult());

	}

	@Test
	public void testFindByFirstNumberAndSecondNumberAndContraintAndFirstStringAndSecondString_Success() {
		FizzBuzzData data = fizzBuzzDataRepo.save(getFizzBuzzData(1));
		data = fizzBuzzDataRepo.findByFirstNumberAndSecondNumberAndContraintAndFirstStringAndSecondString(
				data.getFirstNumber(), data.getSecondNumber(), data.getContraint(), data.getFirstString(),
				data.getSecondString());
		assertNotNull(data);
		assertEquals("fizz,fizzbuzz,fizz,fizzbuzz,fizz", data.getResult());

	}

	@Test
	public void testFindFirstByOrderByRequestHitsDesc_Success() {
		saveRecords();
		FizzBuzzData optData = fizzBuzzDataRepo.findFirstByOrderByRequestHitsDesc().get();
		assertNotNull(optData);
		assertEquals(5, optData.getRequestHits());
	}

	private FizzBuzzData getFizzBuzzData(int count) {

		return FizzBuzzData.builder().firstNumber(1).secondNumber(2).contraint(6).firstString("fizz")
				.secondString("buzz").requestHits(count).result("fizz,fizzbuzz,fizz,fizzbuzz,fizz").build();
	}

	private void saveRecords() {
		for (int i = 0; i < 3; i++) {
			fizzBuzzDataRepo.save(getFizzBuzzData(i + 3));
		}

	}

}
