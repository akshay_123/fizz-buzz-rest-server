package com.roche.fizzbuzz.rest;

import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.roche.fizzbuzz.dto.FizzBuzzRequest;
import com.roche.fizzbuzz.dto.GenericResponse;
import com.roche.fizzbuzz.service.FizzBuzzService;
import com.roche.fizzbuzz.util.CommonConstant;
import com.roche.fizzbuzz.util.ErrorCodes;

@WebMvcTest(FizzBuzzController.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class FizzBuzzControllerTest {

	@MockBean
	private FizzBuzzService fizzBuzzService;

	@Autowired
	private MockMvc mocMvc;

	@Test
	public void testSaveFizzBuzzResultEndpoint_Success() throws Exception {
		when(fizzBuzzService.saveFizzBuzzResult(prepareRequest())).thenReturn(GenericResponse.builder()
				.result("fizz,fizzbuzz,fizz.fizzbuzz,fizz").responseMessage(CommonConstant.SUCCESS_MESSAGE).build());

		this.mocMvc.perform(MockMvcRequestBuilders.post("/v1/roche/fizzbuzz/").contentType(MediaType.APPLICATION_JSON)
				.content("{\n" + "    \"firstNumber\": 1,\n" + "    \"secondNumber\": 2,\n" + "    \"limit\": 5,\n"
						+ "    \"firstString\": \"fizz\",\n" + "    \"secondString\": \"buzz\"\n" + "}"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.responseMessage").value(CommonConstant.SUCCESS_MESSAGE));

	}

	@Test
	public void testSaveFizzBuzzResultEndpoint_InvalidLimit() throws Exception {

		this.mocMvc.perform(MockMvcRequestBuilders.post("/v1/roche/fizzbuzz/").contentType(MediaType.APPLICATION_JSON)
				.content("{\n" + "    \"firstNumber\": 1,\n" + "    \"secondNumber\": 2,\n" + "    \"limit\": 0,\n"
						+ "    \"firstString\": \"fizz\",\n" + "    \"secondString\": \"buzz\"\n" + "}"))
				.andExpect(MockMvcResultMatchers.status().isBadRequest())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errorMessage")
						.value(ErrorCodes.VALIDATION_ERROR.getMessage() + CommonConstant.INVALID_LIMIT_VALUE));

	}

	private FizzBuzzRequest prepareRequest() {
		return FizzBuzzRequest.builder().firstNumber(1).secondNumber(2).limit(5).firstString("fizz")
				.secondString("buzz").build();
	}

}
