package com.roche.fizzbuzz.rest;

import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.roche.fizzbuzz.dto.FizzBuzzRequest;
import com.roche.fizzbuzz.dto.GenericResponse;
import com.roche.fizzbuzz.service.StatisticsService;
import com.roche.fizzbuzz.util.CommonConstant;

@WebMvcTest(StatisticsController.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class StatisticsControllerTest {

	@MockBean
	private StatisticsService statisticsService;

	@Autowired
	private MockMvc mocMvc;

	@Test
	public void testStatisticsEndpoint_Success() throws Exception {
		when(statisticsService.getStatistics()).thenReturn(getStatisticsData());

		this.mocMvc.perform(MockMvcRequestBuilders.get("/v1/fizzbuzz/statistics/"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.responseMessage").value(CommonConstant.SUCCESS_MESSAGE));

	}

	@Test
	public void testStatisticsEndpoint_DataNotAvailable() throws Exception {
		when(statisticsService.getStatistics()).thenReturn(
				GenericResponse.builder().responseMessage(CommonConstant.STAT_DATA_NOT_AVAILABLE_MESSAGE).build());

		this.mocMvc.perform(MockMvcRequestBuilders.get("/v1/fizzbuzz/statistics/"))
				.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers
						.jsonPath("$.responseMessage").value(CommonConstant.STAT_DATA_NOT_AVAILABLE_MESSAGE));

	}

	private GenericResponse getStatisticsData() {
		FizzBuzzRequest statData = FizzBuzzRequest.builder().firstNumber(1).secondNumber(2).limit(5).firstString("fizz")
				.secondString("buzz").build();
		return GenericResponse.builder().data(statData).hits(Integer.toString(5))
				.responseMessage(CommonConstant.SUCCESS_MESSAGE).build();
	}

}
