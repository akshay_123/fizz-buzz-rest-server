package com.roche.fizzbuzz.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "fizz_buzz_data")
@Builder
public class FizzBuzzData {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int localId;
	@Column(name = "first_number", nullable = false)
	private int firstNumber;
	@Column(name = "second_number", nullable = false)
	private int secondNumber;
	@Column(name = "contraint", nullable = false)
	private int contraint;
	@Column(name = "result", nullable = false)
	private String result;
	@Column(name = "first_string", nullable = false)
	private String firstString;
	@Column(name = "second_string", nullable = false)
	private String secondString;
	@Column(name = "request_hits", nullable = false)
	private int requestHits;

}
