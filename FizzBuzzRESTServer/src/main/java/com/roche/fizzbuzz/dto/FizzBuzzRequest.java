package com.roche.fizzbuzz.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

import com.roche.fizzbuzz.util.CommonConstant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FizzBuzzRequest {

	@Positive(message = CommonConstant.INVALID_INTEGER_VALUE)
	private int firstNumber;

	@Positive(message = CommonConstant.INVALID_INTEGER_VALUE)
	private int secondNumber;

	@Positive(message = CommonConstant.INVALID_LIMIT_VALUE)
	private int limit;

	@NotBlank(message = CommonConstant.INVALID_STRING_VALUE)
	@Pattern(regexp = "^(?i)(?!null$).*$", message = CommonConstant.INVALID_STRING_VALUE)
	private String firstString;

	@NotBlank(message = CommonConstant.INVALID_STRING_VALUE)
	@Pattern(regexp = "^(?i)(?!null$).*$", message = CommonConstant.INVALID_STRING_VALUE)
	private String secondString;

}
