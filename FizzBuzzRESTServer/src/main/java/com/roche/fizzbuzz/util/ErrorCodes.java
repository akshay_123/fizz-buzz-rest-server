package com.roche.fizzbuzz.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorCodes {

	VALIDATION_ERROR("400", "Validation Failed:"),
	SYSTEM_ERROR("500", "System error"),
	PROCESSING_ERROR("400", "Exception while processing request");
	private String code;
	private String message;

}
