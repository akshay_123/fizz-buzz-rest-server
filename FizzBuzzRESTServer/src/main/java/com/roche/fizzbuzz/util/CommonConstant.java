package com.roche.fizzbuzz.util;

public interface CommonConstant {
	String INVALID_INTEGER_VALUE = "Value must be a valid integer";
	String INVALID_STRING_VALUE = "Value must be a valid string";
	String INVALID_LIMIT_VALUE = "Limit value must be positive integer";
	String INVALID_STRING_INTEGER_VALUE = "Value must be a valid integer or String";

	// Service Name
	String FIZZ_BUZZ_SERVICE = "FizzBuzzService";
	String STATISTICS_SERVICE = "StatisticsService";

	// success message
	String SUCCESS_MESSAGE = "Request processed successfully";
	String STAT_DATA_NOT_AVAILABLE_MESSAGE = "Statistics data is not available";

}
