package com.roche.fizzbuzz.service;

import com.roche.fizzbuzz.dto.GenericResponse;

public interface StatisticsService {
	GenericResponse getStatistics();

}
