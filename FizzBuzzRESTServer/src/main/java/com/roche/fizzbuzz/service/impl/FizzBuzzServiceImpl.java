package com.roche.fizzbuzz.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.roche.fizzbuzz.dto.ErrorResponse;
import com.roche.fizzbuzz.dto.FizzBuzzRequest;
import com.roche.fizzbuzz.dto.GenericResponse;
import com.roche.fizzbuzz.entity.FizzBuzzData;
import com.roche.fizzbuzz.exception.GenericException;
import com.roche.fizzbuzz.repository.FizzBuzzDataRepo;
import com.roche.fizzbuzz.service.FizzBuzzService;
import com.roche.fizzbuzz.util.CommonConstant;
import com.roche.fizzbuzz.util.ErrorCodes;

import lombok.extern.log4j.Log4j2;

@Service("FizzBuzzService")
@Log4j2
public class FizzBuzzServiceImpl implements FizzBuzzService {
	@Autowired
	private FizzBuzzDataRepo fizzBuzzDataRepo;

	/**
	 * FIZZ-BUZZ Logic
	 */
	@Override
	public GenericResponse saveFizzBuzzResult(FizzBuzzRequest request) {
		GenericResponse response = null;
		List<Object> fizzBuzzLogic = new ArrayList<>();

		// Check if record is exist and save processing time
		response = checkIfRecordyAlreadyExist(request);
		if (response != null)
			return response;

		try {
			for (int i = 1; i <= request.getLimit(); i++) {
				if (i % (request.getFirstNumber() * request.getSecondNumber()) == 0) {
					fizzBuzzLogic.add(request.getFirstString() + request.getSecondString());
				} else if (i % request.getFirstNumber() == 0) {
					fizzBuzzLogic.add(request.getFirstString());

				} else if (i % request.getSecondNumber() == 0) {
					fizzBuzzLogic.add(request.getSecondString());
				} else {
					fizzBuzzLogic.add(i);
				}

			}
			String result = fizzBuzzLogic.stream().map(item -> item.toString()).collect(Collectors.joining(","));
			response = GenericResponse.builder().result(result).responseMessage(CommonConstant.SUCCESS_MESSAGE).build();

			// update the result in DB for showing statistics
			logResult(request, response);

		} catch (Exception e) {
			log.error("In {} Exception Raised: {} ", CommonConstant.FIZZ_BUZZ_SERVICE, e.getMessage());
			throw new GenericException(ErrorResponse.builder().errorCode(ErrorCodes.PROCESSING_ERROR.getCode())
					.errorMessage(ErrorCodes.PROCESSING_ERROR.getMessage()).build());

		}

		return response;
	}

	private GenericResponse checkIfRecordyAlreadyExist(FizzBuzzRequest request) {
		Optional<FizzBuzzData> optData = Optional.ofNullable(
				this.fizzBuzzDataRepo.findByFirstNumberAndSecondNumberAndContraintAndFirstStringAndSecondString(
						request.getFirstNumber(), request.getSecondNumber(), request.getLimit(),
						request.getFirstString(), request.getSecondString()));
		if (optData.isPresent()) {
			FizzBuzzData data = optData.get();
			data.setRequestHits(data.getRequestHits() + 1);
			fizzBuzzDataRepo.save(data);
			return GenericResponse.builder().result(data.getResult()).responseMessage(CommonConstant.SUCCESS_MESSAGE)
					.build();
		}
		return null;
	}

	private void logResult(FizzBuzzRequest request, GenericResponse response) {
		FizzBuzzData data = FizzBuzzData.builder().firstNumber(request.getFirstNumber())
				.secondNumber(request.getSecondNumber()).contraint(request.getLimit())
				.firstString(request.getFirstString()).secondString(request.getSecondString())
				.result(response.getResult()).requestHits(1).build();
		this.fizzBuzzDataRepo.save(data);
	}

}
