package com.roche.fizzbuzz.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.roche.fizzbuzz.dto.ErrorResponse;
import com.roche.fizzbuzz.dto.FizzBuzzRequest;
import com.roche.fizzbuzz.dto.GenericResponse;
import com.roche.fizzbuzz.entity.FizzBuzzData;
import com.roche.fizzbuzz.exception.GenericException;
import com.roche.fizzbuzz.repository.FizzBuzzDataRepo;
import com.roche.fizzbuzz.service.StatisticsService;
import com.roche.fizzbuzz.util.CommonConstant;
import com.roche.fizzbuzz.util.ErrorCodes;

import lombok.extern.log4j.Log4j2;

@Service("StatisticsService")
@Log4j2
public class StatisticsServiceImpl implements StatisticsService {
	@Autowired
	private FizzBuzzDataRepo FizzBuzzDataRepo;

	@Override
	public GenericResponse getStatistics() {
		try {
			Optional<FizzBuzzData> optData = this.FizzBuzzDataRepo.findFirstByOrderByRequestHitsDesc();
			if (optData.isPresent()) {
				FizzBuzzData data = optData.get();
				FizzBuzzRequest statData = FizzBuzzRequest.builder().firstNumber(data.getFirstNumber())
						.secondNumber(data.getSecondNumber()).limit(data.getContraint())
						.firstString(data.getFirstString()).secondString(data.getSecondString()).build();
				return GenericResponse.builder().data(statData).hits(Integer.toString(data.getRequestHits()))
						.responseMessage(CommonConstant.SUCCESS_MESSAGE).build();
			} else {
				return GenericResponse.builder().responseMessage(CommonConstant.STAT_DATA_NOT_AVAILABLE_MESSAGE)
						.build();
			}
		} catch (Exception e) {
			log.error("In {} Exception Raised: {} ", CommonConstant.STATISTICS_SERVICE, e.getMessage());
			throw new GenericException(ErrorResponse.builder().errorCode(ErrorCodes.PROCESSING_ERROR.getCode())
					.errorMessage(ErrorCodes.PROCESSING_ERROR.getMessage()).build());
		}

	}

}
