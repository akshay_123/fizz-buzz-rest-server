package com.roche.fizzbuzz.service;

import com.roche.fizzbuzz.dto.FizzBuzzRequest;
import com.roche.fizzbuzz.dto.GenericResponse;

public interface FizzBuzzService {
	GenericResponse saveFizzBuzzResult(FizzBuzzRequest request);

}
