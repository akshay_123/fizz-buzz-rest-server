package com.roche.fizzbuzz.exception;

import com.roche.fizzbuzz.dto.ErrorResponse;

public class GenericException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ErrorResponse errorResponse;

	public GenericException(ErrorResponse errorResponse) {
		super();
		this.errorResponse = errorResponse;
	}

	public ErrorResponse errorResponse() {
		return errorResponse;
	}

}
