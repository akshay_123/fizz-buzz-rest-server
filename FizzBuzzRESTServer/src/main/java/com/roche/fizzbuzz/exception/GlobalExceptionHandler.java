package com.roche.fizzbuzz.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.roche.fizzbuzz.dto.ErrorResponse;
import com.roche.fizzbuzz.util.CommonConstant;
import com.roche.fizzbuzz.util.ErrorCodes;

import org.springframework.web.bind.MethodArgumentNotValidException;

import lombok.extern.log4j.Log4j2;

@RestControllerAdvice()
@Log4j2
public class GlobalExceptionHandler {
	/**
	 * Handling the Generic custom Exception
	 */
	@ExceptionHandler(GenericException.class)
	public ResponseEntity<?> handleGenericException(GenericException genericException) {
		log.error(genericException);
		return ResponseEntity.ok(genericException.errorResponse);
	}

	/**
	 * Handling the Validation Exception
	 */
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<?> handleMethodArgumentNotValidException(
			MethodArgumentNotValidException methodArgsNotValidException) {
		log.error(methodArgsNotValidException);
		ErrorResponse response = ErrorResponse.builder().errorCode(ErrorCodes.VALIDATION_ERROR.getCode())
				.errorMessage(ErrorCodes.VALIDATION_ERROR.getMessage()
						+ methodArgsNotValidException.getBindingResult().getAllErrors().get(0).getDefaultMessage())
				.build();
		return ResponseEntity.badRequest().body(response);
	}

	/**
	 * Handling the parsing Exception
	 */
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<?> handleParsingException(HttpMessageNotReadableException exception) {
		log.error(exception);
		ErrorResponse response = ErrorResponse.builder().errorCode(ErrorCodes.VALIDATION_ERROR.getCode())
				.errorMessage(ErrorCodes.VALIDATION_ERROR.getMessage() + CommonConstant.INVALID_STRING_INTEGER_VALUE)
				.build();
		return ResponseEntity.badRequest().body(response);
	}

	/**
	 * Handling the Default Exception
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> handleException(Exception exception) {
		log.error(exception);
		ErrorResponse response = ErrorResponse.builder().errorCode(ErrorCodes.SYSTEM_ERROR.getCode())
				.errorMessage(ErrorCodes.SYSTEM_ERROR.getMessage()).build();
		return ResponseEntity.internalServerError().body(response);
	}

}
