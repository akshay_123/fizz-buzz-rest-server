package com.roche.fizzbuzz.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.roche.fizzbuzz.dto.GenericResponse;
import com.roche.fizzbuzz.service.StatisticsService;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/v1/fizzbuzz/statistics")
@Log4j2
public class StatisticsController {

	@Autowired
	private StatisticsService statisticsService;

	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getStatistics() {
		log.info("Request for StatisticService");
		GenericResponse response = statisticsService.getStatistics();
		ObjectMapper mapper = new ObjectMapper();
		try {
			log.info("Response for StatisticService : {}", mapper.writeValueAsString(response));
		} catch (JsonProcessingException jsonProcessingException) {
			log.error("Exception Raised {}", jsonProcessingException.getMessage());
		}
		return ResponseEntity.ok(response);
	}

}
