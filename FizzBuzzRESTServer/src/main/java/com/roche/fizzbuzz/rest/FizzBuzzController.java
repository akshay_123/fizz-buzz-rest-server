package com.roche.fizzbuzz.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.roche.fizzbuzz.dto.FizzBuzzRequest;
import com.roche.fizzbuzz.dto.GenericResponse;
import com.roche.fizzbuzz.service.FizzBuzzService;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/v1/roche/fizzbuzz")
@Validated
@Log4j2
public class FizzBuzzController {

	@Autowired
	private FizzBuzzService fizzBuzzService;

	/**
	 * 
	 * @param FizzBuzz service endpoint
	 * @return
	 */
	@PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveFizzBuzzResult(@RequestBody @Valid FizzBuzzRequest request) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			log.info("Request for fizzBuzzService : {}", mapper.writeValueAsString(request));
		} catch (JsonProcessingException jsonProcessingException) {
			log.error("Exception Raised {}", jsonProcessingException.getMessage());
		}
		GenericResponse response = fizzBuzzService.saveFizzBuzzResult(request);

		try {
			log.info("Response for fizzBuzzService : {}", mapper.writeValueAsString(request));
		} catch (JsonProcessingException jsonProcessingException) {
			log.error("Exception Raised {}", jsonProcessingException.getMessage());
		}
		return ResponseEntity.ok(response);
	}

}
