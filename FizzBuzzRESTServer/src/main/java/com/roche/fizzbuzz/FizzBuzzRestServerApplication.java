package com.roche.fizzbuzz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FizzBuzzRestServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FizzBuzzRestServerApplication.class, args);
	}

}
