package com.roche.fizzbuzz.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.roche.fizzbuzz.entity.FizzBuzzData;

@Repository
public interface FizzBuzzDataRepo extends JpaRepository<FizzBuzzData, Integer> {
	FizzBuzzData findByFirstNumberAndSecondNumberAndContraintAndFirstStringAndSecondString(int firstNumber,
			int secondNumber, int limit, String firstString, String secondString);

	// Find most hit's request
	Optional<FizzBuzzData> findFirstByOrderByRequestHitsDesc();

}
